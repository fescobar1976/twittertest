package com.felipe.twitter.source;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.felipe.twitter.model.Tweets;
import com.felipe.twitter.service.TweetsService;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterException;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;

@Service
public class TwitterReader {

	protected static final int DEFAULT_MINIM_FOLLOWERS = 1500;
	
	@Autowired
	TweetsService tweetsService;
	
	 @PostConstruct
    public void init() {
		  StatusListener listener = new StatusListener(){
		        public void onStatus(Status status) {
		            System.out.println(status.getUser().getName() + " : " + status.getText());
		            System.out.println(status.getUser().getFollowersCount());
		            if(status.getUser().getFollowersCount()>DEFAULT_MINIM_FOLLOWERS) {		            	
			            Tweets tweet=new Tweets();
			            tweet.setLocalizacion(status.getPlace().getCountry());
			            tweet.setTexto(status.getText());
			            tweet.setUsuario(status.getUser().getName());
			            tweet.setValidacion(false);
						tweetsService.save(tweet);
		            }
		            
		        }
		        public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {}
		        public void onTrackLimitationNotice(int numberOfLimitedStatuses) {}
		        public void onException(Exception ex) {
		            ex.printStackTrace();
		        }
				public void onScrubGeo(long userId, long upToStatusId) {
					// TODO Auto-generated method stub
					
				}
				public void onStallWarning(StallWarning warning) {
					// TODO Auto-generated method stub
					
				}
		    };
		    TwitterStream twitterStream = new TwitterStreamFactory().getInstance();
		    FilterQuery fq = new FilterQuery();
		    String keywords[] = {"en","es", "it"};
		    fq.language(keywords);
		    fq.track(keywords);

		    twitterStream.addListener(listener);
		    twitterStream.filter(fq);      
		    twitterStream.addListener(listener);
		    // sample() method internally creates a thread which manipulates TwitterStream and calls these adequate listener methods continuously.
		    twitterStream.sample();
			

    }
	  public static void main(String[] args) throws TwitterException {
/*
		    ConfigurationBuilder cb = new ConfigurationBuilder();
		    cb.setDebugEnabled(true);
		    cb.setOAuthConsumerKey("bbb");
		    cb.setOAuthConsumerSecret("bbb");
		    cb.setOAuthAccessToken("bbb");
		    cb.setOAuthAccessTokenSecret("bbb");

		    TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
		    StatusListener listener = new StatusListener() {

		        public void onStatus(Status status) {
		            System.out.println("@" + status.getUser().getScreenName() + " - " + status.getText());
		        }

		        public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
		            System.out.println("Got a status deletion notice id:" + statusDeletionNotice.getStatusId());
		        }

		        public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
		            System.out.println("Got track limitation notice:" + numberOfLimitedStatuses);
		        }

		        public void onScrubGeo(long userId, long upToStatusId) {
		            System.out.println("Got scrub_geo event userId:" + userId + " upToStatusId:" + upToStatusId);
		        }

		        public void onException(Exception ex) {
		            ex.printStackTrace();
		        }

				public void onStallWarning(StallWarning warning) {
					
				}
		    };

		    FilterQuery fq = new FilterQuery();
		    String keywords[] = {"France", "Germany"};

		    fq.track(keywords);

		    twitterStream.addListener(listener);
		    twitterStream.filter(fq);      
		*/
		}
		 
		  
}
