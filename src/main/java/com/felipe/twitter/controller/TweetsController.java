package com.felipe.twitter.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.felipe.twitter.model.Tweets;
import com.felipe.twitter.service.TweetsService;

@RestController
public class TweetsController {

	@Autowired
	public final TweetsService tweetsService;
	
	public TweetsController(TweetsService tweetsService) {
		this.tweetsService=tweetsService;
	}
	/**	
	 * Consultar los tweets.
	 */
	@RequestMapping(value ="/tweets/list",method = RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> getList(HttpServletRequest request) {
		HttpHeaders responseHeaders = new HttpHeaders();
		
		List<Tweets> lista=tweetsService.getList();
		
		return new ResponseEntity<List<Tweets>>(lista, responseHeaders, HttpStatus.OK);
		

	}
	//Marcar un tweet como validado.
	@RequestMapping(value ="/tweets/validate",method = RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> setValidate(HttpServletRequest request) {
		HttpHeaders responseHeaders = new HttpHeaders();
		
		List<Tweets> lista=tweetsService.getList();
		
		return new ResponseEntity<List<Tweets>>(lista, responseHeaders, HttpStatus.OK);
		

	}
	//Consultar los tweets validados por usuario.
	@RequestMapping(value ="/tweets/validates",method = RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> getValidates(HttpServletRequest request) {
		HttpHeaders responseHeaders = new HttpHeaders();
		
		List<Tweets> lista=tweetsService.getList();
		
		return new ResponseEntity<List<Tweets>>(lista, responseHeaders, HttpStatus.OK);
		

	}
	
	
	//Consultar una clasificaci�n de los N hashtags m�s usados (default 10).
	@RequestMapping(value ="/tweets/hashtagtop",method = RequestMethod.GET,produces = "application/json")
	public ResponseEntity<?> getClasificates(HttpServletRequest request) {
		HttpHeaders responseHeaders = new HttpHeaders();
		
		List<Tweets> lista=tweetsService.getHashTop();
		
		return new ResponseEntity<List<Tweets>>(lista, responseHeaders, HttpStatus.OK);
		

	}
		
}
