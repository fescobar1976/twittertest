package com.felipe.twitter.service;

import java.util.List;

import org.springframework.stereotype.Service;
import com.felipe.twitter.model.Tweets;
import com.felipe.twitter.repository.TweetsRepository;

@Service
public class TweetsService {

	public final TweetsRepository tweetsRepository;
	
	public TweetsService(TweetsRepository tweetsRepository) {
		this.tweetsRepository=tweetsRepository;
	}
	
	public List<Tweets> getList(){
		return (List<Tweets>) tweetsRepository.findAll();
	}
	public Tweets save(Tweets tweet) {
		return tweetsRepository.save(tweet);		
	}
	public Boolean setValidate(Long id){		
		Tweets tt=tweetsRepository.findById(id).get();
		tt.setValidacion(true);
		tweetsRepository.save(tt);
		return true;
	}
	public List<Tweets> getValidates(){
		return (List<Tweets>) tweetsRepository.findUserByTextoLike("lande");
	}
	
	public List<Tweets> getHashTop(){
		
		return (List<Tweets>) tweetsRepository.findUserByTextoLike("lande");
	}
}
